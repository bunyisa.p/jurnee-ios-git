//
//  AuthenticationService.swift
//  jurnee
//
//  Created by ND2020042208 on 30/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import Firebase
import FBSDKLoginKit
import GoogleSignIn
import UIKit

struct AuthenticationService {
    static let shared = AuthenticationService()
    
    func signIn(withFacebook controller: UIViewController, completion: ((Error?) -> Void)?) {
        
        let loginManager: LoginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: controller) { (result, error) in
            if let error = error {
                completion!(error)
                return
            }
            
            if let result = result {
                if result.isCancelled {
                    return
                }
                
                if let accessToken = result.token {
                    let credentail: AuthCredential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
                    
                    self.signIn(withCredentail: credentail) { (authResult, error) in

                        if let error = error {
                            completion!(error)
                            return
                        }
                        
                        self.setUserData(withAuthDataResult: authResult, completion: completion)
                    }
                }
            }
        }
    }

    func signIn(withCredentail credentail: AuthCredential, completion: AuthDataResultCallback?) {
        Auth.auth().signIn(with: credentail, completion: completion)
    }
    
    func setUserData(withAuthDataResult result: AuthDataResult?, completion: ((Error?) -> Void)?) {
        
        guard let uid = result?.user.uid else { return }
                               
        let data = [
            "uid": uid,
            "profileImageUrl": result?.user.photoURL?.absoluteString ?? "",
            "username": result?.user.displayName ?? "",
            "email": result?.user.email ?? "",
            "phoneNo": result?.user.phoneNumber ?? ""
        ] as [String : Any]
        
        COLLECTION_USERS.document(uid).setData(data, completion: completion)
    }
}
