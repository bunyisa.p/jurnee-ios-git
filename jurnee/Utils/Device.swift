//
//  Device.swift
//  jurnee
//
//  Created by ND2020042208 on 30/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import UIKit

class Device {
    
    static var TheCurrentDevice: UIDevice {
        struct Singleton {
            static let device = UIDevice.current
        }
        return Singleton.device
    }
    
    static var TheCurrentDeviceHeight: CGFloat {
        struct Singleton {
            static let height = UIScreen.main.bounds.height
        }
        return Singleton.height
    }
    
    static var hasTopNotch: Bool {
        if #available(iOS 13.0,  *) {
            return UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.top ?? 0 > 20
        } else{
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
    }
    
    static func getWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    static func getHeight() -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    static func isPhone() -> Bool {
        return TheCurrentDevice.userInterfaceIdiom == .phone
    }
    
    static func isPad() -> Bool {
        return TheCurrentDevice.userInterfaceIdiom == .pad
    }
}
