//
//  Storyboard.swift
//  jurnee
//
//  Created by ND2020042208 on 31/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import UIKit

class Storyboard {
    static func getMainStoryBoard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    static func getAuthStoryBoard() -> UIStoryboard {
        return UIStoryboard(name: "Authentication", bundle: nil)
    }
}
