//
//  UIView+Extension.swift
//  jurnee
//
//  Created by ND2020042208 on 30/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import UIKit

extension UIView {
    class func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        let className = String(describing: viewType)
        
        return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
        
    class func loadNib() -> Self {
        return loadNib(self)
    }
}
