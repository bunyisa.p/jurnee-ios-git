//
//  UIColor+Extension.swift
//  jurnee
//
//  Created by ND2020042208 on 30/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import UIKit

extension UIColor {
    class var darkGrayColor: UIColor { return UIColor(named: "darkGray")! }
    class var mediumGrayColor: UIColor { return UIColor(named: "mediumGray")! }
    class var lightGrayColor: UIColor { return UIColor(named: "lightGray")! }
}
