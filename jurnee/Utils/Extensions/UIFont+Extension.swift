//
//  UIFont+Extension.swift
//  jurnee
//
//  Created by ND2020042208 on 30/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func boldSukhumvitOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SukhumvitSet-Bold", size: getFontSize(size: size))!
    }
    
    class func lightSukhumvitOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SukhumvitSet-Light", size: getFontSize(size: size))!
    }
    
    class func mediumSukhumvitOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SukhumvitSet-Medium", size: getFontSize(size: size))!
    }
    
    class func semiBoldSukhumvitOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SukhumvitSet-SemiBold", size: getFontSize(size: size))!
    }
    
    class func regularSukhumvitOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SukhumvitSet-Text", size: getFontSize(size: size))!
    }
    
    class func thinSukhumvitOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SukhumvitSet-Thin", size: getFontSize(size: size))!
    }

    class func getFontSize(size: CGFloat) -> CGFloat {
        switch Device.getWidth() {
        case 320.0:
            return size - 6.0
        case 375.0:
            return size - 3.0
        case 414.0:
            return size
        default:
            return size
        }
    }
}
