//
//  ButtonView.swift
//  jurnee
//
//  Created by ND2020042208 on 29/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import UIKit

@IBDesignable
class ButtonWithCornerView: UIButton {
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    // MARK: - Helpers
    
    func configure() {
        
        backgroundColor = .white
        
        setTitleColor(.darkGrayColor, for: .normal)
        titleLabel?.font = UIFont.semiBoldSukhumvitOfSize(size: 16)
        
        layer.cornerRadius = 28
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = CGSize(width: 1, height: 1)
    }
}
