//
//  HomeController.swift
//  jurnee
//
//  Created by ND2020042208 on 31/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import UIKit
import Firebase

class HomeController: UIViewController {
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authenticateUser()
    }
    
    // MARK: - Selectors
    
    @IBAction func handleSignOut(_ sender: ButtonWithCornerView) {
        do {
            try Auth.auth().signOut()
            presentLoginScreen()
        } catch {
            print("DEBUG: Error signing out..")
        }
    }
    
    
    // MARK: - API
    
    func authenticateUser() {
        if Auth.auth().currentUser?.uid == nil {
            presentLoginScreen()
            
        } else {
            print("DEBUG: User is logged in. \(Auth.auth().currentUser?.uid)")
        }
    }
    
    // MARK: - Helpers
    
    func presentLoginScreen() {
        DispatchQueue.main.async {
            let storyboard = Storyboard.getAuthStoryBoard()
            let controller: LoginController = storyboard.instantiateViewController(identifier: LOGIN_CONTROLLER) as! LoginController
            
            let nav = UINavigationController(rootViewController: controller)
            nav.modalPresentationStyle = .fullScreen
            
            self.present(nav, animated: false, completion: nil)
        }
    }
}
