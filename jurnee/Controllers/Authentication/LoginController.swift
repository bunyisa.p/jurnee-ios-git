//
//  ViewController.swift
//  jurnee
//
//  Created by ND2020042208 on 23/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase

class LoginController: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var signInWithApple: ButtonWithCornerView!
    @IBOutlet weak var signInWithGoogle: ButtonWithCornerView!
    @IBOutlet weak var signInWithFacebook: ButtonWithCornerView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    // MARK: - Selectors
    
    @IBAction func handleSignInWithFacebook(_ sender: ButtonWithCornerView) {
        AuthenticationService.shared.signIn(withFacebook: self) { (error) in
            
            if let error = error {
                self.showError(error.localizedDescription)
                return
            }

            print("DEBUG: Sign In Facebook is successful..")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func handleSignInWithGoogle(_ sender: ButtonWithCornerView) {
        GIDSignIn.sharedInstance()?.signIn()
    }
}

// MARK: - Google Sign In Delegate

extension LoginController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            self.showError(error.localizedDescription)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        
        AuthenticationService.shared.signIn(withCredentail: credential) { (result, error) in
            if let error = error {
                self.showError(error.localizedDescription)
                return
            }
            
            AuthenticationService.shared.setUserData(withAuthDataResult: result) { (error) in
                if let error = error {
                    self.showError(error.localizedDescription)
                    return
                }
            
                print("DEBUG: Sign In Google is successful..")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
