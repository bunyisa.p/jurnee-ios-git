//
//  User.swift
//  jurnee
//
//  Created by ND2020042208 on 31/7/2563 BE.
//  Copyright © 2563 ND2020042208. All rights reserved.
//

import Foundation

struct User {
    let uid: String
    let profileImageUrl: String
    let username: String
    let email: String
    let phoneNo: String
    
    
    init(dictionary: [String : Any]) {
        self.uid = dictionary["uid"] as? String ?? ""
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
        self.username = dictionary["username"] as? String ?? ""
        self.email = dictionary["email"] as? String ?? ""
        self.phoneNo = dictionary["phoneNo"] as? String ?? ""
    }
}
